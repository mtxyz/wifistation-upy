# SPDX-FileCopyrightText: 2024 mtxyz
#
# SPDX-License-Identifier: MIT

"""
`manifest` - MicroPython MIP Manfiest Generator
"""

import json
import sys
import subprocess

from pathlib import Path

# CHANGE THIS to match your project structure.
SRC_PATTERN = "src/wifistation/**/*.py"

# CHANGE THIS to match the location where your code will be hosted!
#  {path} and {version} will be generated from filenames and git commits.
HOST_BASE = "https://codeberg.org/mtxyz/wifistation-upy/raw/commit/{version}/{path}"


def find_repo(start):
    start_path = Path(start).absolute()

    for path in [start_path, *start_path.parents]:
        git_dir = path / ".git"
        if git_dir.is_dir():
            return path

    return None


def main():
    proj_dir = find_repo(__file__) or Path.cwd()

    git_version = subprocess.run(
        ["git", "rev-parse", "--short", "HEAD"],
        capture_output=True,
        check=True,
        text=True,
        cwd=proj_dir,
    ).stdout.strip()

    version = git_version or "DEVEL"

    paths = [path.relative_to(proj_dir) for path in proj_dir.glob(SRC_PATTERN) if path.is_file()]

    urls = [(str(path), HOST_BASE.format(path=path, version=version)) for path in paths]

    manifest = {"urls": urls, "v": version}

    json.dump(manifest, sys.stdout)
    print()


if __name__ == "__main__":
    sys.exit(main())
