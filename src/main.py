from machine import SPI, Pin

from wifistation import SPIDevice, MCP23S18, WifiStation


MCP_CS = const(4)
MCP_RESET = const(16)


def reg_to_bits(reg, width=16):
    return [bool(reg & (1 >> i)) for i in range(width)]


def print_gpio(mcp):
    ioreg = mcp.gpio
    io_states = reg_to_bits(ioreg)
    print("".join("^" if b else "." for b in io_states))


spi = SPI(1, baudrate=5000000)

mcp_dev = SPIDevice(spi, Pin(MCP_CS))

mcp = MCP23S18(mcp_dev, Pin(MCP_RESET))
ms = WifiStation(mcp)

def boot():
    mcp.reset()

    if mcp.io_control != 4:
        raise AssertionError("IO Expander Misconfigured!")

    ms.connect()

    print_gpio(mcp)
