# SPDX-FileCopyrightText: 2016 Scott Shawcroft for Adafruit Industries
# SPDX-FileCopyrightText: 2024 mtxyz
#
# SPDX-License-Identifier: MIT

# pylint: disable=too-few-public-methods

"""
`spi_device` - SPI Bus Device
====================================================

MicroPython module for a generic SPI Device with chip select.

Based on CircuitPython module available at:
https://github.com/adafruit/Adafruit_CircuitPython_BusDevice

* Ported to CircuitPython by: mtxyz (2024)
"""

try:
    from machine import SPI, Pin
except ImportError:
    pass


class SPIDevice:
    def __init__(
        self,
        spi: SPI,
        chip_select: Pin,
        cs_active_value: bool = False,
    ) -> None:
        self.spi = spi

        self.chip_select = chip_select
        self.cs_active_value = cs_active_value
        self.chip_select.init(mode=Pin.OUT, value=not self.cs_active_value)

    def __enter__(self) -> SPI:
        self.chip_select.value(self.cs_active_value)
        return self.spi

    def __exit__(self, *_args) -> bool:
        self.chip_select.value(not self.cs_active_value)
        return False
