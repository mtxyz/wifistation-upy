# SPDX-FileCopyrightText: 2017 Tony DiCola for Adafruit Industries
# SPDX-FileCopyrightText: 2019 Carter Nelson
# SPDX-FileCopyrightText: 2021 Red_M
# SPDX-FileCopyrightText: 2024 mtxyz
#
# SPDX-License-Identifier: MIT

"""
`mcp23sxx`
====================================================

MicroPython module for the MCP23Sxx SPI I/O extender.

* Authors of original CircuitPython Library: Romy Bompart (2020), Red_M (2021)
* Ported to MicroPython by: mtxyz (2024)
"""

from .spi_device import SPIDevice


# shared between both the MCP23S17 class to reduce memory allocations.
# However this is explicitly not thread safe or re-entrant by design!
_OUT_BUFFER = bytearray(4)
_IN_BUFFER = bytearray(4)

# Header to start a reading or writting operation
MCP23SXX_CODE_READ = 0x41
MCP23SXX_CODE_WRITE = 0x40


# pylint: disable=too-few-public-methods
class MCP23SXX:
    """Base class for MCP23Sxx devices."""

    def __init__(
        self,
        device: SPIDevice,
        address: int,
    ) -> None:
        self.cmd_write = MCP23SXX_CODE_WRITE | (address << 1)
        self.cmd_read = MCP23SXX_CODE_READ | (address << 1)
        self._device = device

    def _read_u16le(self, register: int) -> int:
        # Read an unsigned 16 bit little endian value from the specified 8-bit
        # register.
        _OUT_BUFFER[0] = self.cmd_read
        _OUT_BUFFER[1] = register & 0xFF
        with self._device as bus_device:
            bus_device.write_readinto(_OUT_BUFFER, _IN_BUFFER)
        return (_IN_BUFFER[3] << 8) | _IN_BUFFER[2]

    def _write_u16le(self, register: int, value: int) -> None:
        # Write an unsigned 16 bit little endian value to the specified 8-bit
        # register.
        _OUT_BUFFER[0] = self.cmd_write
        _OUT_BUFFER[1] = register & 0xFF
        _OUT_BUFFER[2] = value & 0xFF
        _OUT_BUFFER[3] = (value >> 8) & 0xFF
        with self._device as bus_device:
            bus_device.write(_OUT_BUFFER)

    def _read_u8(self, register: int) -> int:
        # Read an unsigned 8 bit value from the specified 8-bit register.
        _OUT_BUFFER[0] = self.cmd_read
        _OUT_BUFFER[1] = register & 0xFF
        with self._device as bus_device:
            bus_device.write_readinto(_OUT_BUFFER, _IN_BUFFER)
        return _IN_BUFFER[2]

    def _write_u8(self, register: int, value: int) -> None:
        # Write an 8 bit value to the specified 8-bit register.
        _OUT_BUFFER[0] = self.cmd_write
        _OUT_BUFFER[1] = register & 0xFF
        _OUT_BUFFER[2] = value & 0xFF
        with self._device as bus_device:
            bus_device.write(_OUT_BUFFER[0:3])

    def _read_bit(self, register: int, bitn: int) -> bool:
        # Read a single bit in a u16 register.
        return bool(self._read_u16le(register) & (1 << bitn))

    def _write_bit(self, register: int, bitn: int, value: bool) -> None:
        # Write a single bit in a u16 register.
        val = self._read_u16(register)  # Read initial value
        val &= ~(1 << bitn)  # Clear the bit
        val |= value << bitn  # Set the bit if value is True.
        self._write_u16le(val)
