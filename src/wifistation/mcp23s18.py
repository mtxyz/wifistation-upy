# SPDX-FileCopyrightText: 2017 Tony DiCola for Adafruit Industries
# SPDX-FileCopyrightText: 2019 Carter Nelson
# SPDX-FileCopyrightText: 2021 Red_M
# SPDX-FileCopyrightText: 2024 mtxyz
#
# SPDX-License-Identifier: MIT

# pylint: disable=too-many-arguments
# pylint: disable=too-many-public-methods

"""
`mcp23s18`
====================================================

MicroPython module for the MCP23S18 SPI I/O extender.

* Authors of original CircuitPython Library: Tony DiCola, Romy Bompart (2020), Red_M (2021)
* Ported to MicroPython for MCP23S18 by: mtxyz (2024)
"""

from .mcp23sxx import MCP23SXX
from .spi_device import SPIDevice

import time

from machine import Pin
from micropython import const

try:
    from typing import List, Optional
except ImportError:
    pass


_MCP23S18_ADDRESS = const(0x20)
_MCP23S18_IODIRA = const(0x00)
_MCP23S18_IODIRB = const(0x01)
_MCP23S18_IPOLA = const(0x02)
_MCP23S18_IPOLB = const(0x03)
_MCP23S18_GPINTENA = const(0x04)
_MCP23S18_DEFVALA = const(0x06)
_MCP23S18_INTCONA = const(0x08)
_MCP23S18_IOCON = const(0x0A)
_MCP23S18_IOCON_MASK = const(0b01100110)  # Bits 0 and 7 are ignored, bits 3 and 4 are unused.
_MCP23S18_GPPUA = const(0x0C)
_MCP23S18_GPPUB = const(0x0D)
_MCP23S18_GPIOA = const(0x12)
_MCP23S18_GPIOB = const(0x13)
_MCP23S18_INTFA = const(0x0E)
_MCP23S18_INTFB = const(0x0F)
_MCP23S18_INTCAPA = const(0x10)
_MCP23S18_INTCAPB = const(0x11)


# pylint: disable=too-many-arguments
# pylint: disable=too-many-public-methods
class MCP23S18(MCP23SXX):
    """Supports MCP23S18 instance on specified SPI bus and optionally
    at the specified SPI address.
    """

    def __init__(
        self,
        device: SPIDevice,
        reset: Pin,
        address: int = _MCP23S18_ADDRESS,
    ) -> None:
        # For user information
        self.address = address
        self.rst_pin = reset

        super().__init__(device, address)

    def reset(self):
        self.rst_pin.init(mode=Pin.OUT, value=True)
        self.rst_pin.off()
        time.sleep_ms(100)
        self.rst_pin.on()
        time.sleep_ms(100)

        # Reset to all inputs with no pull-ups and no inverted polarity.
        self.iodir = 0xFFFF
        self.gppu = 0x0000
        self.io_control = 0x4  # turn on IRQ Pins as open drain
        self.iopol = 0x0000


    def set_pin(
        self, pin_num: int,
        is_input: Optional[bool] = None,
        pullup: Optional[bool] = None,
        value: Optional[bool] = None
    ):
        if is_input is not None:
            self._write_bit(_MCP23S18_IODIRA, pin_num, is_input)
        if pullup is not None:
            self._write_bit(_MCP23S18_GPPUA, pin_num, pullup)
        if value is not None:
            self._write_bit(_MCP23S18_GPIOA, pin_num, value)

    def get_pin(self, pin_num: int) -> bool:
        return self._read_bit(_MCP23S18_GPIOA, pin_num)

    @property
    def gpio(self) -> int:
        """The GPIO register reflects the value on the port.
        Reading from this register reads the port. Writing to this
        register modifies the Output Latch (OLAT) register.
        """
        return self._read_u16le(_MCP23S18_GPIOA)

    @gpio.setter
    def gpio(self, val: int) -> None:
        self._write_u16le(_MCP23S18_GPIOA, val)

    @property
    def gpioa(self) -> int:
        """The GPIO A register reflects the value on the port.
        Reading from this register reads the port. Writing to this
        register modifies the Output Latch (OLAT) register.
        """
        return self._read_u8(_MCP23S18_GPIOA)

    @gpioa.setter
    def gpioa(self, val: int) -> None:
        self._write_u8(_MCP23S18_GPIOA, val)

    @property
    def gpiob(self) -> int:
        """The GPIO B register reflects the value on the port.
        Reading from this register reads the port. Writing to this
        register modifies the Output Latch (OLAT) register.
        """
        return self._read_u8(_MCP23S18_GPIOB)

    @gpiob.setter
    def gpiob(self, val: int) -> None:
        self._write_u8(_MCP23S18_GPIOB, val)

    @property
    def iodir(self) -> int:
        """The raw IODIR direction register.  Each bit represents
        direction of a pin, either 1 for an input or 0 for an output mode.
        """
        return self._read_u16le(_MCP23S18_IODIRA)

    @iodir.setter
    def iodir(self, val: int) -> None:
        self._write_u16le(_MCP23S18_IODIRA, val)

    @property
    def iodira(self) -> int:
        """The raw IODIR A direction register.  Each bit represents
        direction of a pin, either 1 for an input or 0 for an output mode.
        """
        return self._read_u8(_MCP23S18_IODIRA)

    @iodira.setter
    def iodira(self, val: int) -> None:
        self._write_u8(_MCP23S18_IODIRA, val)

    @property
    def iodirb(self) -> int:
        """The raw IODIR B direction register.  Each bit represents
        direction of a pin, either 1 for an input or 0 for an output mode.
        """
        return self._read_u8(_MCP23S18_IODIRB)

    @iodirb.setter
    def iodirb(self, val: int) -> None:
        self._write_u8(_MCP23S18_IODIRB, val)

    @property
    def gppu(self) -> int:
        """The raw GPPU pull-up register.  Each bit represents
        if a pull-up is enabled on the specified pin (1 = pull-up enabled,
        0 = pull-up disabled).  Note pull-down resistors are NOT supported!
        """
        return self._read_u16le(_MCP23S18_GPPUA)

    @gppu.setter
    def gppu(self, val: int) -> None:
        self._write_u16le(_MCP23S18_GPPUA, val)

    @property
    def gppua(self) -> int:
        """The raw GPPU A pull-up register.  Each bit represents
        if a pull-up is enabled on the specified pin (1 = pull-up enabled,
        0 = pull-up disabled).  Note pull-down resistors are NOT supported!
        """
        return self._read_u8(_MCP23S18_GPPUA)

    @gppua.setter
    def gppua(self, val: int) -> None:
        self._write_u8(_MCP23S18_GPPUA, val)

    @property
    def gppub(self) -> int:
        """The raw GPPU B pull-up register.  Each bit represents
        if a pull-up is enabled on the specified pin (1 = pull-up enabled,
        0 = pull-up disabled).  Note pull-down resistors are NOT supported!
        """
        return self._read_u8(_MCP23S18_GPPUB)

    @gppub.setter
    def gppub(self, val: int) -> None:
        self._write_u8(_MCP23S18_GPPUB, val)

    @property
    def ipol(self) -> int:
        """The raw IPOL output register.  Each bit represents the
        polarity value of the associated pin (0 = normal, 1 = inverted), assuming that
        pin has been configured as an input previously.
        """
        return self._read_u16le(_MCP23S18_IPOLA)

    @ipol.setter
    def ipol(self, val: int) -> None:
        self._write_u16le(_MCP23S18_IPOLA, val)

    @property
    def ipola(self) -> int:
        """The raw IPOL A output register.  Each bit represents the
        polarity value of the associated pin (0 = normal, 1 = inverted), assuming that
        pin has been configured as an input previously.
        """
        return self._read_u8(_MCP23S18_IPOLA)

    @ipola.setter
    def ipola(self, val: int) -> None:
        self._write_u8(_MCP23S18_IPOLA, val)

    @property
    def ipolb(self) -> int:
        """The raw IPOL B output register.  Each bit represents the
        polarity value of the associated pin (0 = normal, 1 = inverted), assuming that
        pin has been configured as an input previously.
        """
        return self._read_u8(_MCP23S18_IPOLB)

    @ipolb.setter
    def ipolb(self, val: int) -> None:
        self._write_u8(_MCP23S18_IPOLB, val)

    @property
    def interrupt_configuration(self) -> int:
        """The raw INTCON interrupt control register. The INTCON register
        controls how the associated pin value is compared for the
        interrupt-on-change feature. If  a  bit  is  set,  the  corresponding
        I/O  pin  is  compared against the associated bit in the DEFVAL
        register. If a bit value is clear, the corresponding I/O pin is
        compared against the previous value.
        """
        return self._read_u16le(_MCP23S18_INTCONA)

    @interrupt_configuration.setter
    def interrupt_configuration(self, val: int) -> None:
        self._write_u16le(_MCP23S18_INTCONA, val)

    @property
    def interrupt_enable(self) -> int:
        """The raw GPINTEN interrupt control register. The GPINTEN register
        controls the interrupt-on-change feature for each pin. If a bit is
        set, the corresponding pin is enabled for interrupt-on-change.
        The DEFVAL and INTCON registers must also be configured if any pins
        are enabled for interrupt-on-change.
        """
        return self._read_u16le(_MCP23S18_GPINTENA)

    @interrupt_enable.setter
    def interrupt_enable(self, val: int) -> None:
        self._write_u16le(_MCP23S18_GPINTENA, val)

    @property
    def default_value(self) -> int:
        """The raw DEFVAL interrupt control register. The default comparison
        value is configured in the DEFVAL register. If enabled (via GPINTEN
        and INTCON) to compare against the DEFVAL register, an opposite value
        on the associated pin will cause an interrupt to occur.
        """
        return self._read_u16le(_MCP23S18_DEFVALA)

    @default_value.setter
    def default_value(self, val: int) -> None:
        self._write_u16le(_MCP23S18_DEFVALA, val)

    @property
    def io_control(self) -> int:
        """The raw IOCON configuration register.
        Bit 0 controls which register clears interrupts. (1 = intcap, 0 = gpio)
          this bit is ignored and always set low.
        Bit 1 controls interrupt polarity (1 = active-high, 0 = active-low).
        Bit 2 is whether irq pin is open drain (1 = open drain, 0 = push-pull).
        Bits 3 and 4 are unused and are always set low.
        Bit 5 is if SPI address pointer auto-increments (1 = no).
        Bit 6 is whether interrupt pins are internally connected (1 = yes).
        Bit 7 is whether registers are all in one bank (1 = no),
          this bit is ignored and always set low.
        """
        return self._read_u8(_MCP23S18_IOCON)

    @io_control.setter
    def io_control(self, val: int) -> None:
        val &= _MCP23S18_IOCON_MASK  # Bits 0 and 7 are ignored, bits 3 and 4 are unused.
        self._write_u8(_MCP23S18_IOCON, val)

    @property
    def int_flag(self) -> List[int]:
        """Returns a list with the pin numbers that caused an interrupt
        port A ----> pins 0-7
        port B ----> pins 8-15
        """
        intf = self._read_u16le(_MCP23S18_INTFA)
        flags = [pin for pin in range(16) if intf & (1 << pin)]
        return flags

    @property
    def int_flaga(self) -> List[int]:
        """Returns a list of pin numbers that caused an interrupt in port A
        pins: 0-7
        """
        intfa = self._read_u8(_MCP23S18_INTFA)
        flags = [pin for pin in range(8) if intfa & (1 << pin)]
        return flags

    @property
    def int_flagb(self) -> List[int]:
        """Returns a list of pin numbers that caused an interrupt in port B
        pins: 8-15
        """
        intfb = self._read_u8(_MCP23S18_INTFB)
        flags = [pin + 8 for pin in range(8) if intfb & (1 << pin)]
        return flags

    def clear_ints(self) -> None:
        """Clears interrupts by reading INTCAP."""
        self._read_u16le(_MCP23S18_INTCAPA)

    def clear_inta(self) -> None:
        """Clears port A interrupts."""
        self._read_u8(_MCP23S18_INTCAPA)

    def clear_intb(self) -> None:
        """Clears port B interrupts."""
        self._read_u8(_MCP23S18_INTCAPB)
