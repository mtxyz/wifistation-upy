# SPDX-FileCopyrightText: 2024 mtxyz
#
# SPDX-License-Identifier: MIT

"""
`wifistation`
====================================================
"""

from machine import Pin

from .mcp23s18 import MCP23S18


RDY_CTRL = const(11)  # Strobe, set high when data has been written.
RDY_STAT = const(8)  # Busy, data ready when high.

ACK_CTRL = const(10)  # Linefeed, hold high until data has been read.
ACK_STAT = const(9)  # Ack, hold data until low.


class WifiStation:
    def __init__(self, mcp: MCP23S18) -> None:
        self.mcp = mcp

    def connect(self):
        self.mcp.reset()
        self._set_dir(False)

        self.mcp.set_pin(RDY_CTRL, is_input=False,
                         pullup=True, value=False)

        self.mcp.set_pin(ACK_CTRL, is_input=False,
                         pullup=True, value=False)

        self.mcp.set_pin(RDY_STAT, is_input=False,
                         pullup=False)

        self.mcp.set_pin(ACK_STAT, is_input=False,
                         pullup=False)

    def _set_dir(self, is_input: bool):
        """Changes the direction of all the data pins to either input or output.
        """
        self.mcp.iodira = 0xFF if is_input else 0
        self.mcp.gppua = 0 if is_input else 0xFF
